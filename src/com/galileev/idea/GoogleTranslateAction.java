package com.galileev.idea;

import com.intellij.ide.BrowserUtil;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataKeys;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.SelectionModel;
import org.jetbrains.annotations.NotNull;

public class GoogleTranslateAction extends AnAction {
  private static final boolean DO_NOT_HONOR_CAMEL_WORDS_SETTINGS = false;

  @Override
  public void actionPerformed(AnActionEvent e) {
    String text = getTextToTranslate(e);
    if (text != null) {
      BrowserUtil.browse("https://translate.google.com/?tl=en#de/en/" + prepareTextForTranslation(text));
    }
  }

  @Override
  public void update(AnActionEvent e) {
    e.getPresentation().setEnabled(isEnabled(e));
  }

  private boolean isEnabled(AnActionEvent e) {
    return getTextToTranslate(e) != null;
  }

  private String getTextToTranslate(AnActionEvent e) {
    Editor editor = e.getData(DataKeys.EDITOR);
    if (editor == null)
      return null;

    SelectionModel selectionModel = editor.getSelectionModel();
    String text = selectionModel.getSelectedText();
    if (text == null) {
      text = getWordAtCaret(selectionModel);
    }
    return text;
  }

  private String getWordAtCaret(SelectionModel selectionModel) {
    selectionModel.selectWordAtCaret(DO_NOT_HONOR_CAMEL_WORDS_SETTINGS);
    String selectedText = selectionModel.getSelectedText();
    selectionModel.removeSelection();
    return selectedText;
  }

  private String prepareTextForTranslation(@NotNull String x) {
    String result = replaceNonLettersWithSpace(x);
    result = separateWordsAtCamelHumps(result);
    return result;
  }

  private static String replaceNonLettersWithSpace(@NotNull String x) {
    return x.replaceAll("[^\\p{Alpha}\\r\\n]+", " ");
  }

  private static String separateWordsAtCamelHumps(@NotNull String x) {
    return x.replaceAll("(\\p{Lower})(\\p{Upper})", "$1 $2");
  }
}
